package ca1.data;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import ca1.business.Bill;
import ca1.business.BillDetail;
import ca1.business.access.SecurityContext;

public class Repository {
	Map <Bill, List<BillDetail>> invoiceRepository = new HashMap<>();
	Map <Bill, List<BillDetail>> detailsRepository = new HashMap<>();
	Map <String, Bill > billRepository = new HashMap<>();
	
	private Bill getBillWithError(String idOperator) {
		Bill b = billRepository.get(idOperator);
		if (b==null) SecurityContext.setLastError("There is no open Bill");
		return b;
	}
	
	private List <BillDetail> getDetailsWithError(Bill b) {
		List <BillDetail> lbd = detailsRepository.get(b);
		if (lbd==null) {
			SecurityContext.setLastError("Bill does not exist!");
			return null;
		}
		return lbd;
	}
	
	public Boolean add(Bill b) {
		if (billRepository.containsKey(b.getOperator())) {
			SecurityContext.setLastError("Bill already exists!");
			return false;
		}
		
		billRepository.put(b.getOperator(), b);
		detailsRepository.put(b, new ArrayList<>());
		return true;
	}
	
	public Boolean addDetail(Bill b, BillDetail d) {
		List <BillDetail> lbd = getDetailsWithError(b);
		if (lbd==null) return false;					
		
		lbd.add(d);
		return true;
	}
	
	public Boolean removeDetail(Bill b, BillDetail d) {
		List <BillDetail> lbd = getDetailsWithError(b);
		if (lbd==null) return false;			
		
		if (!lbd.contains(d)) {
			SecurityContext.setLastError("Detail does not exist!");
			return false;
		}
	
		lbd.remove(d);
		return true;
	}	
	
	public Bill getBill(String idOperator) {
		return getBillWithError(idOperator);
	}
	
	public List<BillDetail> getDetails(Bill b) {

		return Collections.unmodifiableList(getDetailsWithError(b));
	}
	
	public Boolean removeToInvoice(String idOperator) {
		Bill b = getBillWithError(idOperator);
		if (b==null) return false;
		
		List <BillDetail> lbd = getDetailsWithError(b);
		if (lbd==null) return false;
		
		billRepository.remove(idOperator);
		detailsRepository.remove(b);
		invoiceRepository.put(b,lbd);
		return true;
	}
	
	public List <Bill> getInvoiceList(String idClient) {
		
		return invoiceRepository.keySet().stream().filter(d -> (d.getClient().equals(idClient))).collect(Collectors.toList());
	}
	public List <BillDetail> getInvoiceDetails(Bill b) {
		
		return invoiceRepository.get(b);
	}
}
