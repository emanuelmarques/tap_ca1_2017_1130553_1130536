package ca1.business;

public class BillDetail {
	String product;
	int quantity;
	BillDetail(String product, int quantity) {
		this.product=product;
		this.quantity=quantity;
	}
	
	public String toString() {
		return "Product: "+product.toString()+" Quantity: "+quantity;
	}
	
	public boolean equals(Object o) {
		if (!(o instanceof BillDetail)) return false;
		BillDetail b = (BillDetail)o; 
		return (product.equals(b.product) && (quantity==b.quantity));
	}	
}
