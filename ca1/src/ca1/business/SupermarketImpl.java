package ca1.business;

import java.util.List;

import ca1.business.access.SecurityContext;
import ca1.business.annotations.Authenticated;
import ca1.business.annotations.Authenticated.Role;
import ca1.data.Repository;

public class SupermarketImpl implements ISupermarket {
	static int bills=0;
	private Repository repo = new Repository();	

	/*
	 * @startuml
	 * -> ISupermarket : createBill(idOperator,idClient)
	 * ISupermarket -> Bill : new Bill(bills,idOperator,idClient)
	 * ISupermarket <-- Bill : new Bill b
	 * ISupermarket -> Repository : add(b)	 * 
	 * alt ret == true
	 * ISupermarket <-- Repository : ret
	 * <-- ISupermarket : Bill b
	 * end
	 * <-- ISupermarket : null
	 * @enduml
	 */
	@Override
	@Authenticated(Role.CASHIER)
	public Bill createBill(String idOperator, String idClient) {

		Bill b = new Bill(bills,idOperator,idClient);
		bills++;
		if (repo.add(b)) return b;
		return null;
	}

	/*
	 * @startuml
	 * -> ISupermarket : getOpenBill(idOperator,idClient)
	 * ISupermarket -> Repository : getBill(idOperator)
	 * Repository -> Repository : getBillWithError(idOperator)
	 * Repository -> ArchitecturalEnforcement : ruleOneEnforcement()
	 * 
	 * note left
	 * 		With the repository calling himself, it is a 
	 * 		call outside the business layer, therefore triggering
	 * 		the rule one of architectural enforcement. This causes 
	 * 		the application to not follow the "normal" behaviour and
	 * 		immediately stop.
	 * end note
	 * 
	 * ArchitecturalEnforcement -> ArchitecturalEnforcement : haltApplication(error)
	 * ArchitecturalEnforcement -> SecurityContext : setLastError(error)
	 * <-- ArchitecturalEnforcement : print(error)
	 * <-- ArchitecturalEnforcement : System.exit(0)
	 * 
	 * @enduml
	 */
	@Override
	@Authenticated(Role.CASHIER)
	public Bill getOpenBill(String idOperator) {
		Bill b = repo.getBill(idOperator);
		return b;
	}

	@Override
	@Authenticated(Role.CASHIER)
	public List<BillDetail> getOpenBillDetails(Bill b) {
		List<BillDetail> lbd = repo.getDetails(b);
		return lbd;
	}

	@Override
	@Authenticated(Role.CASHIER)
	public Boolean addProductToOpenBill(Bill b, String product, int quantity) {
		return repo.addDetail(b, new BillDetail(product, quantity));
	}

	/*
	 * @startuml
	 * note left of SupermarketImplJP
	 * 		SupermarketImplJP represents the joinpoint (jp)
	 * 		of SupermarketImpl class in which the first
	 * 		aspect is triggered (Profiler)
	 * end note
	 * -> SupermarketImplJP : removeProductFromOpenBill(bill, product, quantity)
	 * SupermarketImplJP -> Profiler : profile(jp)
	 * note left of Profiler
	 *		The profile method is triggered by all
	 *		the executions of a method of the 
	 *		business layer annotated with the
	 * 		@Authenticated annotation, called 
	 * 		from outside the business layer.
	 * 		This method also triggers another aspects,
	 * 		but the profiler has precedence defined over
	 * 		Authorization, and then RemoveBillDetails
	 * end note
	 * Profiler -> SupermarketImplJP : finalResult = proceed()
	 * note left
	 * 		This advice proceeds with 
	 * 		the execution of the intersected joinpoint
	 * 		but records the result of it in order 
	 * 		to profile it
	 * end note
	 * SupermarketImplJP -> Authorization : authorization(role, jp)
	 * note right
	 * 		All methods with @Authenticated annotation
	 * 		are adviced by the Authorization aspect in order
	 * 		to validate if the user has authorization to 
	 * 		use this functionality.
	 * end note
	 * Authorization -> SecurityContext : check(role)
	 * Authorization <-- SecurityContext : result
	 * alt result == true
	 * 		Authorization -> SupermarketImplJP : proceed()
	 * 		SupermarketImplJP -> Repository : removeDetail(bill, billdetail)
	 * 		Repository -> Repository : getDetailsWithError(bill)
	 * 		Repository <-- Repository : listBillDetails
	 * 		Repository -> RemoveBillDetails : contains(billdetail, listBillDetails)
	 * 		note right
	 * 			Using reflection, the aspect RemoveBillDetails,
	 * 			verifies if listBillDetails contains a BillDetail with the same product
	 * 			of the received BillDetail (billdetail).
	 * 			This aspect is triggered by the execution of the method removeDetail of the Repository class
	 * 			and the methods contains and remove of java.util.List when called in the flow of removeDetail
	 *  	end note	
	 *  	RemoveBillDetails --> Repository : resultContains
	 *  	alt resultContains == true
	 *  		Repository -> RemoveBillDetails : remove(billdetail, listBillDetails)
	 *  		note right
	 * 				Using reflection, the aspect RemoveBillDetails
	 * 				tries to remove the specified quantity from the existent BillDetail. 
	 * 				If the existent BillDetail contains the exact specified quantity, then the 
	 * 				entire BillDetail is removed from listBillDetails
	 *  		end note
	 *  		RemoveBillDetails --> Repository : resultRemove
	 *			Repository --> SupermarketImplJP : true
	 *			Profiler <-- SupermarketImplJP : true
	 *  	else resultContains == false
	 *  		Repository -> SecurityContext : setLastError(errorMessage)
	 *  		Repository --> SupermarketImplJP : false
	 *  		SupermarketImplJP --> Profiler : false
	 *  	end
	 * else result == false
	 * 		Authorization --> SupermarketImplJP : false
	 * 		note right
	 * 			Using reflection, the Authorization aspect verifies
	 * 			the return type (Object or Boolean) of the method
	 * 			and returns the respective error. In this case: false
	 * 		end note
	 * 		Profiler <-- SupermarketImplJP : false
	 * end
	 * <-- Profiler : finalResult
	 * note right
	 * 		The Profiler aspect registers the success or failure
	 * 		associating it with the current user username and 
	 * 		then returns the result unaltered
	 * end note
	 * @enduml
	 */
	@Override
	@Authenticated(Role.MANAGER)
	public Boolean removeProductFromOpenBill(Bill b, String product, int quantity) {
		return repo.removeDetail(b, new BillDetail(product, quantity));
	}
	
	/*
	 * @startuml
	 * -> ISupermarket : checkout(idOperator)
	 * ISupermarket -> Repository : removeToInvoice(idOperator)
	 * Repository -> Repository : getBillWithError(idOperator)
	 *  
	 * alt b == null
	 * Repository -> SecurityContext : setLastError("There is no open Bill")
	 * end
	 * 
	 * Repository -->Repository : ret(bill)
	 * 
	 * alt bill == null
	 * Repository --> ISupermarket : ret(false)
	 * end
	 * 
	 * Repository -> Repository : getDetailsWithError(b) 
	 * 
	 * note right
	 * 'b' is the bill
	 * end note
	 * 
	 * alt lbd == null
	 * Repository -> SecurityContext : setLastError("Bill does not exist")
	 * end
	 * 
	 * Repository -->Repository : ret(lbd)
	 * 
	 * note right
	 * lbd is the list of bill details
	 * end note
	 * 
	 * alt lbd == null
	 * Repository --> ISupermarket : ret(false)
	 * end
	 * 
	 * Repository ->Map : remove(idOperator)
	 * 
	 * note right
	 * Removes the bills of that operator
	 * end note
	 * 
	 * Repository ->Map : remove(b)
	 * 
	 * note right
	 * Removes the details of that bill
	 * end note
	 * 
	 * Repository -> Checkout : transformInvoice(key, value, idOperator)
	 * 
	 * alt value == null || value.isEmpty()
	 * Checkout -->Repository : ret (false)
	 * end
	 * 
	 * Checkout -> Checkout : convertToInvoice(key)
	 * 
	 * note right 
	 * The Bill is converted into an invoice using reflection. 
	 * This allows to access the bill's private fields and modify them.
	 * end note
	 * 
	 * Checkout -> Checkout : sendEmail(bill, details)
	 * Checkout -> EmailSender : sendEmail("Thanks for shopping with us!", msg)
	 * 
	 * note right 
	 * This method uses an external API to send an email
	 * end note
	 * 
	 * EmailSender --> Checkout : ret(true)
	 * Checkout -->Repository : ret(true)
	 * Repository ->Map : put(b, lbd)
	 * Repository -->ISupermarket : ret(true)
	 * <-- ISupermarket : true
	 * @enduml
	 */
	@Override
	@Authenticated(Role.CASHIER)
	public Boolean checkout(String idOperator) {
		return repo.removeToInvoice(idOperator);
	}

	@Override
	@Authenticated(Role.CLIENT)
	public List<Bill> getInvoiceList(String idClient) {
		return repo.getInvoiceList(idClient);
	}

	@Override
	@Authenticated(Role.CLIENT)
	public List<BillDetail> getInvoiceDetails(Bill b) {
		return repo.getInvoiceDetails(b);
	}

	@Override
	public Boolean setUserCredentials(String idOperator, Role role) {
		SecurityContext.set(idOperator, role);
		return true;
	}

	@Override
	public String getLastError() {
		return SecurityContext.getLastError();
	}

}
