package ca1.business;

import java.util.List;

import ca1.business.annotations.Authenticated;

public interface ISupermarket {
	
	/**
	 * Creates a new bill for a client
	 * 
	 * @param idOperator is the operator id
	 * @param idClient is the client being billed
	 * @return the bill identification object
	 */
	public Bill createBill(String idOperator, String idClient);
	/**
	 * Gets the operators open bill in the system if it exists
	 * 
	 * @param idOperator is the operator id 
	 * @return the bill identification object or null if the bill does not exist
	 */
	public Bill getOpenBill(String idOperator);
	/**
	 * 
	 * @param b is the bill
	 * @return the list of all the details of the bill
	 */
	public List <BillDetail> getOpenBillDetails(Bill b);
	/**
	 *  
	 * @param b is the bill
	 * @param product is the product id
	 * @param quantity
	 * @return the product that corresponds to ibProduct or null 
	 */
	public Boolean addProductToOpenBill(Bill b, String product, int quantity);
	/**
	 * removes <b>quantity<b> of product <b>product<b> from open Bill <b>b<b> 
	 * 
	 * @param b
	 * @param product
	 * @param quantity
	 * @return the product that corresponds to ibProduct or null
	 */
	public Boolean removeProductFromOpenBill(Bill b, String product, int quantity);
	/**
	 * Checks out an open bill
	 * 
	 * @param idOperator is the operator id
	 * @return an invoice or null if the bill is not open or does not exist
	 */
	public Boolean checkout(String idOperator);
	
	/**
	 * 
	 * @return list of all the invoices
	 */
	public List <Bill> getInvoiceList(String idClient);
	/**
	 * 
	 * @return the bill corresponding to the invoice
	 */
	public List <BillDetail> getInvoiceDetails(Bill b);
	/**
	 * Sets a new operator as the <b>principal<b>.
	 * 
	 * @param the operator identification
	 * @param the role of the principal
	 * @return false if the principal does not exist in the system, true otherwise
	 */	
	public Boolean setUserCredentials(String idOperator, Authenticated.Role role);
	/**
	 * Returns the last error that occurred on a call to the business layer
	 * 
	 * @return error description
	 */	
	public String getLastError();
}
