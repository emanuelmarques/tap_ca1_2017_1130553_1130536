package ca1.business.access;

import java.util.Deque;
import java.util.ArrayDeque;

import ca1.business.annotations.Authenticated;

public class SecurityContext {
	public static String operator = null;
	public static Authenticated.Role role = null;	
	public static Deque <String> errorLog = null;
	
	public static void invalidate() {
		operator = null;
		role = null;
		errorLog = null;
	}
	public static void set(String operator, Authenticated.Role role ) {
		SecurityContext.operator = operator;
		SecurityContext.role = role;
		errorLog = new ArrayDeque<>();
	}
	public static boolean check(Authenticated auth) throws Exception {
		if (SecurityContext.role==null) return false;
		
		int minimumAuthentication = auth.value().ordinal();
		int thisAuthentication = SecurityContext.role.ordinal();
		
		if (thisAuthentication>=minimumAuthentication) return true;
				
		setLastError("This should only be performed by "+auth.value()+" or above");
		return false;
	}
	public static String getPrincipalRole() {
		if (role==null) return "Undefined";
		return role.toString();
	}
	public static String getPrincipalOperator() {
		return operator;
	}	
	public static void setLastError(String error) {
		errorLog.addLast(error);
	}
	public static String getLastError() {
		return errorLog.getLast();
	}	
}
