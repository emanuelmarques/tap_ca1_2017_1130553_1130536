package ca1.business;

public class SupermarketFactory {
	
	public ISupermarket createSupermarket() {
		return new SupermarketImpl();
	}	
}
