package ca1.business;

public class Bill {

	int id;
	String idOperator;
	String idClient;
	
	Bill(int id, String idOperator, String idClient) {
		this.id = id;
		this.idOperator=idOperator;
		this.idClient=idClient;
	}
	
	public String toString() {
		return "ID: "+id+" Client: "+idClient+" Operator: "+idOperator;
	}
	
	public boolean equals(Object o) {
		if (!(o instanceof Bill)) return false;
		Bill b = (Bill)o; 
		return (id==b.id);
	}
	
	public int getId() {
		return id;
	}	
	public String getOperator() {
		return idOperator;
	}
	public String getClient() {
		return idClient;
	}
}
