package ca1.business.annotations;

import java.lang.annotation.*;

@Retention(RetentionPolicy.RUNTIME)
public @interface Authenticated {
	public enum Role { CLIENT, CASHIER, MANAGER }
	Role value();
}
