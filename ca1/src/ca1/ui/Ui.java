package ca1.ui;

import java.util.List;
import java.util.Optional;
import java.util.Scanner;

import ca1.business.*;
import ca1.business.access.SecurityContext;
import ca1.business.annotations.Authenticated;

public class Ui {
	private ISupermarket supermarket;
	private Scanner in;
	Bill openBill;	

	public void execute() {
		supermarket = new SupermarketFactory().createSupermarket();
		in = new Scanner(System.in);
		boolean error;

		login();
		int userChoice;
		boolean quit = false;
		do {			
			System.out.println("Operator: "+SecurityContext.getPrincipalOperator()+" Role: "+SecurityContext.getPrincipalRole());
			System.out.println("1. Create and open a new Bill");
			System.out.println("2. Get open Bill");
			System.out.println("3. View open Bill details");
			System.out.println("4. Add Product to Bill");
			System.out.println("5. Remove Product from Bill");
			System.out.println("6. Checkout");
			System.out.println("7. View Invoice bill");
			
			System.out.println("9. login");
			System.out.print("Your choice, 0 to quit: ");
			userChoice = in.nextInt();
			System.out.println();

			error = false;
			switch (userChoice) {
			case 1:
				error = createAndOpenBill();

				break;
			case 2:
				error = getOpenBill();

				break;
			case 3:
				error = viewOpenBillDetails();

				break;
			case 4:
				error = addProductToBill();

				break;
			case 5:
				error = removeProductFromBill();

				break;
			case 6:
				error = checkOut();
				break;
			case 7:
				error = getInvoiceBill();
				break;				
			case 9:
				login();				
				break;
			case 0:
				quit = true;				
				break;
			default:
				System.out.println("Wrong choice.");
				break;
			}
			if (error) {
				System.out.println("Error: " + supermarket.getLastError());
			}
			System.out.println();
		} while (!quit);
		in.close();
	}

	private boolean createAndOpenBill() {
		String idClient = getStringFromUser("Client Id:");
		openBill = supermarket.createBill(SecurityContext.getPrincipalOperator(), idClient);
		return (openBill==null);
	}
	
	private boolean getOpenBill() {
		openBill = supermarket.getOpenBill(SecurityContext.getPrincipalOperator());
		if (openBill!=null) System.out.println(openBill);
		return (openBill==null);
	}
	
	private boolean viewOpenBillDetails() {
		openBill = supermarket.getOpenBill(SecurityContext.getPrincipalOperator());
		if (openBill==null) return true;
		List <BillDetail> lbd = supermarket.getOpenBillDetails(openBill);
		if (lbd==null) return true;
		showBillDetails(openBill,lbd);
		return false;
	}
	
	private boolean addProductToBill() {
		String product = getStringFromUser("Product:");
		int quantity = getIntFromUser("Quantity: ");
		return !supermarket.addProductToOpenBill(openBill, product, quantity);
	}
	

	private boolean removeProductFromBill() {
		String product = getStringFromUser("Product: ");
		int quantity = getIntFromUser("Quantity: ");
		return !supermarket.removeProductFromOpenBill(openBill, product, quantity);
	}
	

	private boolean checkOut() {
		return !supermarket.checkout(SecurityContext.getPrincipalOperator());
	}
	
	private boolean getInvoiceBill() {
		String client = getStringFromUser("Client Id: ");
		List <Bill> lb = supermarket.getInvoiceList(client);
		if (lb==null || lb.isEmpty()) {
			SecurityContext.setLastError("There are no invoices!");
			return true;
		}
		showInvoices(lb, client);
		Optional <Bill> opb = null;
		do {
			int id = getIntFromUser("Invoice id: ");
			opb = lb.stream().filter(b -> (b.getId()==id)).findFirst();
			
		} while (!opb.isPresent());
		showBillDetails(opb.get(),supermarket.getInvoiceDetails(opb.get()));
		return false;
	}	

	private Boolean login() {
		openBill = null;
		String operator = getStringFromUser("Operator: ");
		Authenticated.Role role = Authenticated.Role.valueOf(getStringFromUser("Role(CLIENT;CASHIER;MANAGER): "));

		return !supermarket.setUserCredentials(operator, role);
	}
	
	private String getStringFromUser(String what) {
		System.out.println(what);
		return in.next();
	}

	private int getIntFromUser(String what) {
		System.out.println(what + ": ");
		return in.nextInt();
	}
	private void showInvoices(List<Bill> lb, String c) {
		System.out.println("Invoice List for client "+c);
		lb.forEach(d -> System.out.println(d.toString()));
	}
	private void showBillDetails(Bill b, List<BillDetail> lbd) {
		System.out.println("Details for Bill: "+b);
		lbd.forEach(d -> System.out.println(d.toString()));
	}

	public static void main(String[] args) {
		new Ui().execute();
	}
}
