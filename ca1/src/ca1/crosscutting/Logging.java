package ca1.crosscutting;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.time.Instant;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;

import ca1.business.Bill;
import ca1.business.BillDetail;
import ca1.business.access.SecurityContext;

/**
 * @author Emanuel Marques (1130553@isep.ipp.pt)
 * @author Joao Neves (1130536@isep.ipp.pt)
 *
 */
@Aspect
public class Logging {

	/**
	 * The log file path
	 */
	private static final String LOG_FILE_PATH = "Logger.txt";

	/**
	 * Intercepts the 'removeDetail' method in the 'ca1.data.Repository' class
	 * 
	 * @param bill
	 *            The Bill
	 * @param details
	 *            The Bill's details
	 */
	@Pointcut("execution(public boolean ca1.data.Repository.removeDetail(..))&& args(bill, details)")
	public void removeBillDetails(Bill bill, BillDetail details) {
	}

	/**
	 * Logs the outcome of the method 'removeBillDetail'
	 * 
	 * @param bill
	 *            The Bill
	 * @param details
	 *            The Bill's details
	 * @param pjp
	 *            The Proceeding Join Point
	 * @return
	 */
	@Around("removeBillDetails(bill, details)")
	public boolean logRemoveDetails(Bill bill, BillDetail details, ProceedingJoinPoint pjp) {
		boolean result = false;
		try {
			result = (boolean) pjp.proceed();

			if (!result) {
				this.Log("Failed to remove details. Bill: " + bill.toString() + "" + " Bill Details: "
						+ details.toString() + " Operator: " + SecurityContext.operator + " " + "Timestamp: "
						+ Instant.now());
			} else {
				this.Log("Successfully removed Bill details! Bill: " + bill.toString() + "" + " Bill Details: "
						+ details.toString() + " Operator: " + SecurityContext.operator + " " + "Timestamp: "
						+ Instant.now());
			}
		} catch (Throwable e) {
			this.Log("Failed to remove details. Error: " + e.getMessage() + " Timestamp: " + Instant.now());
		}
		return result;

	}

	/**
	 * Logs a message into the log file
	 * 
	 * @param log
	 *            The message to Log
	 */
	private void Log(String log) {
		try {

			File file = new File(LOG_FILE_PATH);
			FileWriter fileWriter = new FileWriter(file, true);
			BufferedWriter bufferFileWriter = new BufferedWriter(fileWriter);
			fileWriter.append(log);
			bufferFileWriter.newLine();
			bufferFileWriter.close();

		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
