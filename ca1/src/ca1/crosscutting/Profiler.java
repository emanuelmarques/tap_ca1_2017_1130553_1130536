package ca1.crosscutting;

import java.util.HashMap;
import java.util.Map;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.DeclarePrecedence;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;

import ca1.business.access.SecurityContext;

/**
 * @author Emanuel Marques (1130553@isep.ipp.pt)
 * @author Joao Neves (1130536@isep.ipp.pt)
 */
@Aspect
@DeclarePrecedence("Profiler, Authorization")
public class Profiler {

//	When EXIT, 
//	For each user logged in during runtime:
//
//	Profile calls
//		FROM outside business layer
//		TO business layer
//		WITH Authenticated Annotation
//
//		- Number of succesful calls 
//		- Number of calls that resulted in error 
//  GARANTIR QUE ASPECTO S� CORRE DEPOIS DE AUTORIZHATION
	
	/**
	 * This collection keeps track of the number of successful calls each user made. 
	 * The key (String) is the username and the value (Integer) is the correspondent successful calls count.
	 */
	private static Map<String, Integer> successfulCallsCountByUser = new HashMap<>();
	
	/**
	 * This collection keeps track of the number of failed calls each user made. 
	 * The key (String) is the username and the value (Integer) is the correspondent failed calls count.
	 */
	private static Map<String, Integer> failedCallsCountByUser = new HashMap<>();
	
	/**
	 * Intercepts all the calls made to a method of the business layer (business package and subpackages),
	 * annotated with the \@Authenticated annotation, made from outside the business layer. 
	 */
	@Pointcut("execution(@ca1.business.annotations.Authenticated * ca1.business..*.*(..)) && !call(* ca1.business..*.*(..))")
	public void callToAuthenticatedBusinessLayerFromOutsideBusinessLayer(){}
	
	/**
	 * Intercepts all the executions of the method "execute" of the class "ca1.ui.Ui" 
	 */
	@Pointcut("execution(public void ca1.ui.Ui.execute())")
	public void whenExit(){}
	
	/**
	 * This advice replaces the functionality of the intersected method in order to profile the outcome of it.
	 * The advice does not alter the functionality of the original method, it just profiles the outcome.
	 * @param pjp the joinpoint in which this advice is applied, in order to be able to proceed with the execution of the original method.
	 * @return the return is the same as it would be from the original method
	 * @throws Throwable in case the original method throws an exception.
	 */
	@Around("callToAuthenticatedBusinessLayerFromOutsideBusinessLayer()")
	public Object profile(ProceedingJoinPoint pjp) throws Throwable {
		Object result = pjp.proceed();
		
		Signature signature = pjp.getSignature();
		Class<?> returnType = ((MethodSignature) signature).getReturnType();
		if (java.lang.Boolean.class == returnType){
			boolean boolResult = (boolean) result;
			if(boolResult){
				incOneToSuccessfulCallsCount();
				return result;
			}
			incOneToFailedCallsCount();
			return result;
		}
		
		if(result == null){
			incOneToFailedCallsCount();
			return result;
		}
		
		incOneToSuccessfulCallsCount();
		return result;
	}
	
	/**
	 * This advice prints the profiling data gathered for each user during the runtime of the application.
	 */
	@After("whenExit()")
	public void whenExitPrintProfiling(){
		System.out.println("The count of successful calls from ouside the business package to @Authenticated methods inside business package was the following: ");
		for(Map.Entry<String, Integer> entry : successfulCallsCountByUser.entrySet())
		    System.out.println("User: "+entry.getKey()+", made "+entry.getValue()+" successful calls.");
		
		System.out.println("The count of failed calls from outside the business package to @Authenticated methods inside business package was the following: ");
		for(Map.Entry<String, Integer> entry : failedCallsCountByUser.entrySet())
		    System.out.println("User: "+entry.getKey()+", made "+entry.getValue()+" failed calls.");	
	}
	
	/**
	 * Increments one to the failed calls count of the current user.
	 */
	private void incOneToFailedCallsCount(){
		String operator = SecurityContext.operator;
		if(failedCallsCountByUser.containsKey(operator)){
			int failedCount = failedCallsCountByUser.get(operator);
			failedCallsCountByUser.replace(operator, failedCount+1);			
		}else{
			failedCallsCountByUser.put(operator, 1);
		}
	}
	
	/**
	 * Increments one to the successful calls count of the current user.
	 */
	private void incOneToSuccessfulCallsCount(){
		String operator = SecurityContext.operator;
		if(successfulCallsCountByUser.containsKey(operator)){
			int successfulCount = successfulCallsCountByUser.get(operator);
			successfulCallsCountByUser.replace(operator, successfulCount+1);			
		}else{
			successfulCallsCountByUser.put(operator, 1);
		}
	}
}
