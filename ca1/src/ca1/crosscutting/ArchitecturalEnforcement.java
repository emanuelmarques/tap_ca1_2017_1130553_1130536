package ca1.crosscutting;

import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;

import ca1.business.access.SecurityContext;

//1) Error: No method on the data package can be called from outside the business layer.
//2) Error: No business method can be called except the ISupermarket implementations can
//be called from the user interface.
//3) Error: The business interface cannot be instantiated outside the business package.
//4) Warning: Every method which implements ISupermarket should have an
//@Authenticated annotation.

/**
 * @author Emanuel Marques (1130553@isep.ipp.pt)
 * @author Joao Neves (1130536@isep.ipp.pt)
 */
@Aspect
public class ArchitecturalEnforcement {
	
	/**
	 * Intercepts all methods called within a class of the ca1.crosscutting package
	 */
	@Pointcut("within(ca1.crosscutting.*)")
	public void crossCutting(){}
	
	/**
	 * Intercepts all calls to methods on the data package, 
	 * made from outside the business layer (except for crosscutting methods)
	 */
	@Pointcut("(call(* ca1.data.*.*(..)) && "
			+ "!within(ca1.business..*)) && "
			+ "!crossCutting()")
	public void ruleOne(){}
	
	/**
	 * Intercepts all calls to business methods that are not implementations of ISupermarket, 
	 * called from the user interface
	 */
	@Pointcut("(call(* ca1.business..*(..)) && "
			+ "!call(* ca1.business.ISupermarket+.*(..)) && "
			+ "within(ca1.ui.Ui))")
	public void ruleTwo(){}
	
	/**
	 * Intercepts all instantiations of ISupermarket interface outside the business package
	 * (except for crosscutting methods)
	 */
	@Pointcut("(call(ca1.business.ISupermarket+.new(..)) && "
			+ "!within(ca1.business.*)) && "
			+ "!crossCutting()")
	public void ruleThree(){}

	/**
	 * Intercepts all methods that implement ISupermarket interface that do not have the \@Authenticated annotation.
	 */
	@Pointcut("(execution(* ca1.business.ISupermarket+.*(..)) && "
			+ "!execution(@ca1.business.annotations.Authenticated * *(..)))")
	public void ruleFour(){}
	
	/**
	 * This advice stops the application in case of a method on the data package being called from outside the business layer.
	 * A failure to comply with this rule is intersected by the pointcut "ruleOne".
	 */
	@Before("ruleOne()")
	public void ruleOneEnforcement(){
		String error = "No method on the data package can be called from outside the business layer.";
		haltApplication(error);
	}
	
	/**
	 * This advice stops the application in case of a business method being called from the user interface,
	 * except for ISupermarket implementations.
	 * A failure to comply with this rule is intersected by the pointcut "ruleTwo"
	 */
	@Before("ruleTwo()")
	public void ruleTwoEnforcement(){
		String error = "No business method can be called except the ISupermarket implementations can be called from the user interface.";
		haltApplication(error);
	}
	
	/**
	 * This advice stops the application in case of an instantiation of the business interface outside the business package.
	 * A failure to comply with this rule is intersected by the pointcut "ruleThree"
	 */
	@Before("ruleThree()")
	public void ruleThreeEnforcement(){
		String error = "The business interface cannot be instantiated outside the business package.";
		haltApplication(error);
	}
	
	/**
	 * This advice warns the user through a printed message that every method which implements ISupermarket should have an @Authenticated annotation
	 */
	@After("ruleFour()")
	public void ruleFourWarning(){
		System.out.println("Warning: Every method which implements ISupermarket should have an @Authenticated annotation.");
	}
	
	
	/**
	 * This method saves a received error and halts the application
	 * @param error an error that has ocurred and should force the application to stop
	 */
	private void haltApplication(String error){
		try{
			SecurityContext.setLastError(error);
		}catch(NullPointerException ex){
			//just print the message bellow
		}
		
		System.out.println(error);
		System.exit(0);
	}
}
