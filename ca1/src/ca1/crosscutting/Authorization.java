package ca1.crosscutting;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;

import ca1.business.access.SecurityContext;
import ca1.business.annotations.Authenticated;

/**
 * @author Emanuel Marques (1130553@isep.ipp.pt)
 * @author Joao Neves (1130536@isep.ipp.pt)
 *
 */
@Aspect
public class Authorization {

	/**
	 * Intercepts all methods with an 'Authenticated' annotation
	 * 
	 * @param role
	 *            The method's minimum role
	 * 
	 */
	@Pointcut("execution(@ca1.business.annotations.Authenticated * *(..)) && @annotation(role)")
	public void authenticated(Authenticated role) {
	}

	/**
	 * Verifies the current user's role and checks if it has permissions for
	 * that action
	 * 
	 * @param role
	 *            The method's minimum role
	 * @param pjp
	 *            The method's Proceeding Joint Point
	 * @return The method's return value if permissions are correct. Otherwise,
	 *         it returns false or null
	 * @throws Throwable
	 */
	@Around("authenticated(role)")
	public Object authorization(Authenticated role, ProceedingJoinPoint pjp) throws Throwable {
		if (SecurityContext.check(role)) {
			return pjp.proceed();
		}

		Signature signature = pjp.getSignature();
		Class<?> returnType = ((MethodSignature) signature).getReturnType();
		if (returnType == java.lang.Boolean.class) {
			return false;
		}

		return null;
	}
}
