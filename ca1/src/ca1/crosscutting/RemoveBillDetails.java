package ca1.crosscutting;

import java.lang.reflect.Field;
import java.util.List;

import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.DeclarePrecedence;
import org.aspectj.lang.annotation.Pointcut;

import ca1.business.Bill;
import ca1.business.BillDetail;

/**
 * @author Emanuel Marques (1130553@isep.ipp.pt)
 * @author Joao Neves (1130536@isep.ipp.pt)
 */
@Aspect
@DeclarePrecedence("Profiler, Authorization, RemoveBillDetails")
public class RemoveBillDetails {

	/**
	 * Intercepts the execution of the method removeDetail of the class ca1.data.Repository and captures its arguments.
	 * @param bill The Bill argument of the intersected method.
	 * @param bdetail The BillDetail argument of the intersected method.
	 */
	@Pointcut("execution(public Boolean ca1.data.Repository.removeDetail(ca1.business.Bill, ca1.business.BillDetail)) && "
			+ "args(bill, bdetail)")
	public void removeDetail(Bill bill, BillDetail bdetail){		
	}
	
	/**
	 * Intercepts all calls made to the method contains of java.util.List 
	 * in the execution flow captured by the removeDetail pointcut, and captures the target list.
	 * @param bill The Bill argument captured on the removeDetail pointcut
	 * @param bdetail The BillDetail argument captured on the removeDetail pointcut
	 * @param list The target list of which the method contains is called.
	 */
	@Pointcut("call(public boolean java.util.List.contains(java.lang.Object)) && "	
			+ "cflow(removeDetail(bill, bdetail)) && "
			+ "target(list)")
	public void containsInsideRemoveDetail(Bill bill, BillDetail bdetail, Object list) {
	}

	/**
	 * Intercepts all calls made to the method remove of java.util.List
	 * in the execution flow captured by the removeDetail pointcut, and captures the target list.
	 * @param bill The Bill argument captured on the removeDetail pointcut
	 * @param bdetail The BillDetail argument captured on the removeDetail pointcut
	 * @param list The target list of which the method remove is called.
	 */
	@Pointcut("call(public boolean java.util.List.remove(java.lang.Object)) && "
			+ "cflow(removeDetail(bill, bdetail)) && "
			+ "target(list)")
	public void removeInsideRemoveDetail(Bill bill, BillDetail bdetail, Object list) {
	}
	
	/**
	 * Intercepts all joinpoints inside a class of the crosscutting package
	 */
	@Pointcut("within(ca1.crosscutting.*)")
	public void crossCutting(){}
	
	/**
	 * This advice replaces the contains method intersected by the containsInsideRemoveDetail pointcut,
	 * except if it is captured in a crosscutting class context.
	 * The contains method when adviced by this advice will
	 * verifies if there is any BillDetail on list that matches the product of the received BillDetail.
	 * Furthermore, it will also check if that BillDetail contains more or equal quantity than the received BillDetail. 
	 * @param bill the received Bill by the removeDetail pointcut
	 * @param bdetail the received BillDetail by the removeDetail pointcut to be compared with the BillDetails on list.
	 * @param list the list on which the contains method is called
	 * @return True if a corresponding BillDetail is found or False otherwise.
	 */
	@Around("containsInsideRemoveDetail(bill, bdetail, list) && !crossCutting()")
	public boolean contains(Bill bill, BillDetail bdetail, Object list){
		List<BillDetail> lbd = (List<BillDetail>) list;
		try {
			Field productField = BillDetail.class.getDeclaredField("product");
			productField.setAccessible(true);
			Field quantityField = BillDetail.class.getDeclaredField("quantity");
			quantityField.setAccessible(true);
			
			try {
				String product = (String) productField.get(bdetail);
				int quantity = (int) quantityField.get(bdetail);
				for(BillDetail detail : lbd){
					String thisProd = (String) productField.get(detail);
					int thisQuant = (int) quantityField.get(detail);
					if(product.equals(thisProd) && thisQuant >= quantity){
						return true;
					}
				}
				
				return false;
			} catch (IllegalArgumentException | IllegalAccessException e) {
				return false;
			}
		} catch (NoSuchFieldException | SecurityException e) {
			return false;
		}
	}
	
	/**
	 * This advice replaces the remove method intersected by the removeInsideRemoveDetail pointcut,
	 * except if it is captured in a crosscutting class context.
	 * The remove method when adviced by this advice will
	 * find a BillDetail on list that has the same product of the received BillDetail (bdetail), and then
	 * it will remove the specified quantity on bdetail from the BillDetail of list.
	 * If the quantity is exactly the same, then the entire BillDetail object will be removed from list. 
	 * @param bill the received Bill by the removeDetail pointcut
	 * @param bdetail the received BillDetail by the removeDetail pointcut, which contains the data needed to remove the specific quantity of a specific product from the list.
	 * @param list the list on which the remove method is called
	 * @return True if the quantity specified on bdetail is succesfully removed from a BillDetail on list, or false otherwise.
	 */
	@Around("removeInsideRemoveDetail(bill, bdetail, list) &&  !crossCutting()")
	public boolean remove(Bill bill, BillDetail bdetail, Object list){
		List<BillDetail> lbd = (List<BillDetail>) list;
		try {
			Field productField = BillDetail.class.getDeclaredField("product");
			productField.setAccessible(true);
			Field quantityField = BillDetail.class.getDeclaredField("quantity");
			quantityField.setAccessible(true);
			
			String product;
			int quantity;
			try {
				product = (String) productField.get(bdetail);
				quantity = (int) quantityField.get(bdetail);
			} catch (IllegalArgumentException | IllegalAccessException e1) {
				return false;
			}
			
			for(BillDetail detail : lbd){
				try {
					String thisProd = (String) productField.get(detail);
					int thisQuantity = (int) quantityField.get(detail);
					
					if(thisProd.equals(product)){
						if(thisQuantity > quantity){
							quantityField.setInt(detail, thisQuantity-quantity);
							return true;
						}
						if(thisQuantity == quantity){
							lbd.remove(detail);
							return true;
						}
					}
				} catch (IllegalArgumentException | IllegalAccessException e) {
					return false;
				}
			}
			
		} catch (NoSuchFieldException | SecurityException e) {
			return false;
		}
		return false;
	}
}
