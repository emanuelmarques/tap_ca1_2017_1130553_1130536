package ca1.crosscutting;

import java.util.*;
import javax.mail.*;
import javax.mail.internet.*;

/**
 * @author Emanuel Marques (1130553@isep.ipp.pt)
 * @author Joao Neves (1130536@isep.ipp.pt)
 *
 */
public class EmailSender {

	/**
	 * Destination email address
	 */
	public static final String to = "1130553@isep.ipp.pt";

	/**
	 * Account username
	 */
	public static final String username = "mei.isep.2016@gmail.com";

	/**
	 * Account Password
	 */
	public static final String password = "ASDqwe123";

	/**
	 * Sends an email
	 * 
	 * @param subject
	 *            Email subject
	 * @param msg
	 *            Email body message
	 * @return
	 */
	public static boolean sendEmail(String subject, String msg) {

		Properties props = new Properties();

		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.host", "smtp.gmail.com");
		props.put("mail.smtp.port", "587");

		Session session = Session.getInstance(props, new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(username, password);
			}
		});

		try {

			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress("mei.isep.2016@gmail.com"));
			message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(to));
			message.setSubject(subject);
			message.setText(msg);

			Transport.send(message);

			System.out.println("Email Sent!");

		} catch (MessagingException e) {
			System.out.println("Unable to send email :(");
			return false;
		}
		return true;
	}
}
