package ca1.crosscutting;

import java.lang.reflect.Field;
import java.util.List;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;

import ca1.business.Bill;
import ca1.business.BillDetail;

/**
 * @author Emanuel Marques (1130553@isep.ipp.pt)
 * @author Joao Neves (1130536@isep.ipp.pt)
 *
 */
@Aspect
public class Checkout {

	/**
	 * Intercepts the 'removeToInvoice' method in 'ca1.data.Repository'
	 * 
	 * @param idOperator
	 *            The id of the operator
	 */
	@Pointcut("execution(public Boolean ca1.data.Repository.removeToInvoice(String)) && args(idOperator)")
	public void checkout(String idOperator) {
	}

	/**
	 * Intercepts the 'put' method of the 'Map' class
	 * 
	 * @param key
	 *            The Bill
	 * @param value
	 *            The Bill's list of details
	 */
	@Pointcut("call(public * java.util.Map.put(..)) && args(key, value)")
	public void invoiceUpdate(Bill key, List<BillDetail> value) {
	}

	/**
	 * Intercepts the 'put' method when called inside the 'removeToInvoice'
	 * method.
	 * 
	 * @param key
	 *            The Bill
	 * @param value
	 *            The Bill's list of details
	 * @param idOperator
	 *            The operator's id
	 */
	@Pointcut("invoiceUpdate(key, value)  && cflow(checkout(idOperator))")
	public void updateInvoiceOnCheckout(Bill key, List<BillDetail> value, String idOperator) {
	}

	/**
	 * Converts the Bill into an invoice and sends an email
	 * 
	 * @param pjp
	 *            The method's Proceeding Join Point
	 * @param key
	 *            The Bill
	 * @param value
	 *            The Bill's list of details
	 * @param idOperator
	 *            The operator's id
	 * @return
	 */
	@Around("updateInvoiceOnCheckout(key, value, idOperator)")
	public boolean transformInvoice(ProceedingJoinPoint pjp, Bill key, List<BillDetail> value, String idOperator) {
		if (value == null || value.isEmpty()) {
			return false;
		}
		
		Bill invoice = convertToInvoice(key);
		
		try {
			pjp.proceed();
		} catch (Throwable e) {
			return false;
		}
		
		sendEmail(invoice, value);
		return true;
	}

	/**
	 * Sends an email
	 * 
	 * @param bill
	 *            The Bill
	 * @param details
	 *            The Bill's detail list
	 * @return True if success, False otherwise
	 */
	private boolean sendEmail(Bill bill, List<BillDetail> details) {
		String msg = "Thanks for shopping with us! \n\n" + "Bill:\n  " + bill.toString() + "\n\nDetails:";
		
		for (BillDetail billDetail : details) {
			msg+= "\n    - "+billDetail.toString();
		}				

		EmailSender.sendEmail("Thanks for shopping with us!", msg);
		return true;
	}

	/**
	 * Converts the bill into an invoice
	 * 
	 * @param bill
	 *            Bill to convert
	 * @return Invoice
	 */
	private Bill convertToInvoice(Bill bill) {
		try {
			Field id = bill.getClass().getDeclaredField("id");
			id.setAccessible(true);
			id.setInt(bill, bill.getId() + 10000);
			return bill;
		} catch (Exception e) {
			return null;
		}
	}
}
